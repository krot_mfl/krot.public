program console1;

{$APPTYPE CONSOLE}

uses
  System, SysUtils, StrUtils, ShellApi;


var
  file_in:TextFile;
  json:String;
  tmp_string:String;

  pos_beg:Integer;
  pos_end:Integer;
  i:Integer;
  row_data_start:String;
  row_data_end:String;
  row:String;
  lines : array [1..1000] of String;
  Separator:String;
  num_lines:Integer;
  sensors: array [1..1000] of array [1..1000] of integer;
  num_sensors:Integer;
  tmp_str:String;

  size_x :Integer;
  size_y :Integer;
  y      :Integer;
  x      :Integer;
  out_file_name:String;
  file_out:TextFile;
  parameters:String;

begin

    if (ParamCount() < 2) then
    begin
        WriteLn(' External analyzer of krotw32 zoom data.');
        WriteLn(' Usage: example_cpp.exe path_to_krotw32_exe path_to_json_data_file');
        Exit; 
    end;

    AssignFile(file_in, ParamStr(2)); // �������� �������� ����� � �������� ����������
//    AssignFile(file_in, 'kro17D.tmp.json');
    {$I-} // ���������� �������� ������ �����-������
    Reset(file_in); // �������� ����� ��� ������
    {$I+} // ��������� �������� ������ �����-������

    if IOResult<>0 then // ���� ���� ������ ��������, ��
    begin
         WriteLn('������ �������� ����� C:\1.TXT');
         Exit; // ����� �� ��������� ��� ������ �������� �����
    end;

    While not EOF(file_in) do // ���� �� ����� ����� ������ ����:
    begin
       ReadLn(file_in, tmp_string); // ������ �� ����� ������
       json := json + tmp_string;
    end;

    CloseFile(file_in); // ������� ����

    // ������ ����� �������������� ������ �� ������� (��������� � �.�.)
    for i := 1 to Length(json) do begin
       if json[i] < #32 then json[i] := ' '; //S[i] := #32;
    end;

    // �������� ���� �������� �� ������
    json := StringReplace(json, ' ', '', [rfReplaceAll]);

    row_data_start := 'dataRow';
    pos_beg := Pos(row_data_start, json) + Length(row_data_start);
    Delete(json, 1, pos_beg);

    row_data_start := '[[';
    pos_beg := Pos(row_data_start, json) + Length(row_data_start);
    row_data_end := ']]';
    pos_end := Pos(row_data_end, json);
    row := Copy(json, pos_beg, pos_end - pos_beg);

    // ������� ������ 
    num_lines := 0;
    Separator := '],[';
    pos_end := Pos(Separator, row);
    While pos_end > 0 do
    begin
      Inc(num_lines);
      lines[num_lines]:= Copy(row, 1, pos_end-1);
      Delete(row, 1, pos_end-1 + Length(Separator));
      pos_end := Pos(Separator, row);
    end;
    Inc(num_lines);
    lines[num_lines] := Copy(row, 1, Length(row));

    // ������� �� ����� ��������
    for i := 1 to num_lines do
    begin
       num_sensors := 0;
       Separator := ',';
       pos_end := Pos(Separator, lines[i]);
       While pos_end > 0 do
       begin
         Inc(num_sensors);
         //���� ���������, ����������� ����� �������.
         sensors[i][num_sensors] :=  StrToInt(Copy(lines[i], 1, pos_end-1));
         Delete(lines[i], 1, pos_end-1 + Length(Separator));
         pos_end := Pos(Separator, lines[i]);
       end;
       Inc(num_sensors);
       sensors[i][num_sensors] :=  StrToInt(lines[i]);
    end;

    // calculate needed parameters
    size_x := num_lines;
    size_y := num_sensors;
    y := size_y div 4;
    x := size_x div 4;


    // make output file name from input file name
    out_file_name := ParamStr(2) + '.out';

    // open output json file
    AssignFile(file_out,out_file_name);
    Rewrite(file_out);

    // print json answer
    Writeln(file_out,'{');
    Writeln(file_out,'  "status": "ok"');
    Writeln(file_out,'  "result": [');

    Writeln(file_out,'    [', 0          , ', ', 0          , ', ', x, ', ', y, ', ', 80, '],');
    Writeln(file_out,'    [', size_x - x , ', ', 0          , ', ', x, ', ', y, ', ', 70, '],');
    Writeln(file_out,'    [', 0          , ', ', size_y - y , ', ', x, ', ', y, ', ', 60, '],');
    Writeln(file_out,'    [', size_x - x , ', ', size_y - y , ', ', x, ', ', y, ', ', 50, ']' );

    Writeln(file_out,'  ],');
    Writeln(file_out,'  "zoomDataFormat": 1');
    Writeln(file_out,'}');

    Flush(file_out);
    CloseFile(file_out);

    // run krotw32 with answer
    parameters := ' -z' + ParamStr(2) + '!' + out_file_name;
    ShellExecute(0, 'Open', PChar(ParamStr(1)), PChar(parameters), nil, 9);

end.
