"""
Example of python webservice client for KrotW32 zoom data analyzing.
(C) 2016 by Vitaly Bogomolov mail@vitaly-bogomolov.ru
"""
import sys, os, json, urllib, urllib2, time

def proc_input(input_file_name, krotw32, weburl):
    """
    Load input json file, take results of analyzing and save output json file
    """
    answer = ""

    try:
        data = urllib.urlencode({'data' : open(input_file_name, 'r').read(), })
        req = urllib2.Request("%s/api/put" % weburl, data)

        url = urllib2.urlopen(req).geturl()
        #print "Data url: %s" % url

        req = urllib2.Request(url)
        answer = urllib2.urlopen(req).read()

        while answer in ['inprogress']:
            print answer
            time.sleep(1)
            answer = urllib2.urlopen(req).read()

    except Exception, e:
        data = {"zoomDataFormat": 1}
        data["status"] = "error"
        data["error"] = "%s" % e
        answer = json.dumps(data, indent=2)

    output_file_name = "%s.out" % input_file_name
    output_file = open(output_file_name, 'w')
    output_file.write(answer)
    output_file.close()

    # call krotw32
    os.system("%s -z%s!%s" % (krotw32, input_file_name, output_file_name))

if __name__ == "__main__":
    if len(sys.argv) > 3:
        if len(sys.argv) == 5:
            os.environ["http_proxy"] = sys.argv[4]
        proc_input(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        import doctest
        doctest.testmod()
