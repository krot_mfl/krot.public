program Example0;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils, ShellApi;

procedure Analyse;
var
  sInp: String;
  sOut: String;
  sTemp: String;

  fJSONInp: TextFile; // ������� JSON-����
  fJSONOut: TextFile; // �������� JSON-����

  sJSONOutName: String;

  sxy: String;
  sdx, sdy: String;

  i: Integer;

  x, y: Integer;

  Size_x: Integer;
  Size_y: Integer;
begin
  try
    // �������� �������� JSON-�����
    AssignFile(fJSONInp, AnsiUpperCase(ParamStr(2)));
    FileMode := fmOpenRead;
    Reset(fJSONInp);

    sJSONOutName  := ExtractFilePath(ParamStr(2)) + 'Temp.txt';

    // �������� ��������� JSON-�����
    AssignFile(fJSONOut, sJSONOutName);
    Rewrite(fJSONOut);

    // ���������� ������ ������ �� �������� JSON-�����
    ReadLn(fJSONInp, sInp);

    Size_x := 0;
    Size_y := 1;

    // �������� ��������
    while Pos(' ', sInp) > 0 do
      Delete(sInp, Pos(' ', sInp), 1);

    // ����������� ���������� �����
    Delete(sInp, 1, Pos('[[', sInp) + 2);
    sTemp := Copy(sInp, 1, Pos(']]', sInp) + 1);

    for i := Low(sTemp) to High(sTemp) do
      if sTemp[i] = ']' then
        Inc(Size_x);

    // ����������� ���������� �������� (��� ������ ������)
    sTemp := Copy(sInp, 1, Pos(']', sInp));

    for i := Low(sTemp) to High(sTemp) do
      if sTemp[i] = ',' then
        Inc(Size_y);

    // ������������ ������� ������ �� ������ ����������� ������

    x := Size_x div 4;
    y := Size_y div 4;

    sxy := IntToStr(x) + ',' + IntToStr(y) + ',50]';

    sdx := IntToStr(Size_x - x) + ',';
    sdy := IntToStr(Size_y - y) + ',';

    // ������������ ������ ������ ��������� JSON-�����
    sOut := '{"status":"ok","result":[[0,0,' + sxy + ',[' + sdx + '0,' + sxy +
      ',[0,' + sdy + sxy + ',[' + sdx + sdy + sxy + '],"zoomDataFormat":1}';

    // ������ ������ � �������� JSON-����
    Writeln(fJSONOut, sOut);

    // ������ ��������� Krotw32 � �����������
    ShellExecute(0, 'Open', PWideChar(ParamStr(1)),
      PWideChar('-z' + ParamStr(2) + '!' + sJSONOutName), nil, 9);
  finally
    CloseFile(fJSONInp);
    CloseFile(fJSONOut);
  end;
end;

begin
  try
    if ParamCount < 2 then
      begin
        WriteLn('Usage:');
        WriteLn('   Example.exe "Path to KrotW32\" "zoom_data.json\"');
        WriteLn('Press <ENTER> to terminate the program.');
        ReadLn;
      end
    else
      Analyse;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;

end.
