import sys, os, tempfile, json

def analyze(row_data):
    size_x = len(row_data)
    size_y = len(row_data[0])
    y = size_y / 4
    x = size_x / 4

    return [
        [0,          0,          x, y, 40, 1],
        [size_x - x, 0,          x, y, 50, 1],
        [0,          size_y - y, x, y, 60, 1],
        [size_x - x, size_y - y, x, y, 70, 1],
    ]

input_file_name = sys.argv[1]

answer = {"zoomDataFormat": 1}
answer["result"] = analyze(json.loads(open(input_file_name, 'r').read())["dataRow"])
answer["status"] = "ok"

output_file = tempfile.NamedTemporaryFile(delete=False)
output_file_name = output_file.name
json.dump(answer, output_file)
output_file.close()
os.system("%s -z%s!%s" % (sys.argv[2], input_file_name, output_file_name))
