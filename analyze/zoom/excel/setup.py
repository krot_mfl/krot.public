import os
import sys
from distutils.core import setup
import py2exe  # noqa: F401

sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(__file__)), 'source'))

setup(
    # options = {'py2exe': {'bundle_files': 1}},
    # zipfile=None,
    windows=[{
      "script": "source/2excel.py",
    }]
)
