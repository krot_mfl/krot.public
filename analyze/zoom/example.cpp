// ***********************************************************
// Example of cpp program for KrotW32 zoom data analyzing.
// (C) 2016 by Vitaly Bogomolov mail@vitaly-bogomolov.ru
// 
// cl /EHsc example.cpp 
// ***********************************************************

#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>
#include <vector>
#include <algorithm>

#include <stdlib.h> 

using namespace std;

vector<string> split(const string &str, const string &delim) {
    vector<string> elems;
    string::size_type pos, lastPos = 0;
    string item;

    pos = str.find(delim, lastPos);
    while(pos != string::npos) {
        item = str.substr(lastPos, pos-lastPos);
        elems.push_back(item);
        lastPos = pos + delim.size();
        pos = str.find(delim, lastPos);
    }

    item = str.substr(lastPos);
    elems.push_back(item);
    return elems;
}

string extract_between(const string text, const string mark_start, const string mark_end) {
    string::size_type pos1 = text.find(mark_start) + mark_start.size();
    string::size_type pos2 = text.find(mark_end, pos1);
    return text.substr(pos1, pos2-pos1); 
}

int main(int argc, char *argv[]) 
{ 
    if (argc < 2) {
        cout << "External analyzer of krotw32 zoom data.\nUsage: example_cpp.exe path_to_json_data_file path_to_krotw32_exe" << endl;
        return 0; 
    }

    // open json file
    ifstream file(argv[1]);
    string json;

    // reserve buffer
    file.seekg(0, ios::end);   
    json.reserve(file.tellg());
    file.seekg(0, ios::beg);

    // read file
    json.assign((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
    // clear whitespaces
    json.erase(remove_if(json.begin(), json.end(), isspace), json.end());

    // extract substring for dataRow
    string row = extract_between(json, "\"dataRow\":[[", "]]"); 

    string arr_x = extract_between(json, "\"sizeX\":", ",");
    string arr_y = extract_between(json, "\"sizeY\":", ",");

    // split dataRow by lines
    vector<string> lines = split(row, "],[");
    // split first line by sensors values
    vector<string> sensors = split(lines.front(), ",");

    // calculate needed parameters

    //int size_x = lines.size();
    //int size_y = sensors.size();
    int size_x = atoi(arr_x.c_str());
    int size_y = atoi(arr_y.c_str());

    int y = size_y / 4;
    int x = size_x / 4;

    // make output file name from input file name
    string out_file = argv[1];
    out_file.append(".out");

    // open output json file
    ofstream out(out_file.c_str());

    // print json answer
    out << "{" << endl;
    out << "  \"status\": \"ok\"," << endl;
    out << "  \"result\": [" << endl;

    out << "    [" << 0          << ", " << 0          << ", " << x << ", " << y << ", " << 80 << ", 1]," << endl;
    out << "    [" << size_x - x << ", " << 0          << ", " << x << ", " << y << ", " << 70 << ", 1]," << endl;
    out << "    [" << 0          << ", " << size_y - y << ", " << x << ", " << y << ", " << 60 << ", 1]," << endl;
    out << "    [" << size_x - x << ", " << size_y - y << ", " << x << ", " << y << ", " << 50 << ", 1]"  << endl;

    out << "  ]," << endl;
    out << "  \"zoomDataFormat\": 2" << endl;
    out << "}" << endl;

    out.close();

    //std::cout << "x: " << x << " y: " << y;
    //char tmp[2];
    //std::cin.getline (tmp, 2);

    // run krotw32 with answer
    string cmd = argv[2];
    cmd.append(" -z");
    cmd.append(argv[1]);
    cmd.append("!");
    cmd.append(out_file);
    system(cmd.c_str());

    //cout << cmd  << endl;
    return 0; 
}
