program Example;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils, System.Classes, DBXJSON, ShellApi;

procedure Analyse;
var
  sOut: TStringList;
  sInp: TStringList;

  JSON: TJSONObject;

  sFileOut: String;

  sxy: String;
  sdx, sdy: String;

  x, y: Integer;

  Size_x: Integer;
  Size_y: Integer;
begin
  sFileOut := ExtractFilePath(ParamStr(2)) + 'Temp.txt';

  sInp := TStringList.Create;
  sOut := TStringList.Create;

  try
    sInp.LoadFromFile(AnsiUpperCase(ParamStr(2)));

    JSON := TJSONObject.ParseJSONValue(sInp.Text) as TJSONObject;

    Size_x := (JSON.Get('dataRow').JsonValue as TJSONArray).Size;
    Size_y := ((JSON.Get('dataRow').JsonValue as TJSONArray).Get(0)
      as TJSONArray).Size;

    x := Size_x div 4;
    y := Size_y div 4;

    sxy := IntToStr(x) + ',' + IntToStr(y) + ',50]';

    sdx := IntToStr(Size_x - x) + ',';
    sdy := IntToStr(Size_y - y) + ',';

    sOut.Text := '{"status":"ok","result":[[0,0,' + sxy + ',[' + sdx + '0,' +
      sxy + ',[0,' + sdy + sxy + ',[' + sdx + sdy + sxy +
      '],"zoomDataFormat":1}';

    sOut.SaveToFile(sFileOut);

    ShellExecute(0, 'Open', PWideChar(ParamStr(1)),
      PWideChar('-z' + ParamStr(2) + '!' + sFileOut), nil, 9);
  finally
    sInp.Free;
    sOut.Free;
    JSON.Free;
  end;
end;

begin
  try
    if ParamCount < 2 then
      begin
        WriteLn('Usage:');
        WriteLn('   Example.exe "Path to KrotW32\" "zoom_data.json\"');
        WriteLn('Press <ENTER> to terminate the program.');
        ReadLn;
      end
    else
      Analyse;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;

end.
