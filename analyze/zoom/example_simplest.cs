﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Microsoft.Samples.JsonSerialization
{

    [DataContract]
    class Answer
    {
        [DataMember]
        internal int zoomDataFormat;

        [DataMember]
        internal String dataType;

        [DataMember]
        internal String status;

        [DataMember]
        internal int[][] dataRow;
        
        [DataMember]
        internal int[][] dataFilt;
    }

    [DataContract]
    class calc_data
    {
        [DataMember]
        internal int zoomDataFormat;

        [DataMember]
        internal int[][] result;

        [DataMember]
        internal String status;

        public void analyze(Answer data)
        {
            int size_x = data.dataRow.Length;
            int size_y = data.dataRow[0].Length;
            int y = size_y / 4;
            int x = size_x / 4;

            this.result = new int[][]
              { 
                new int[] {0,          0,          x, y, 50},
                new int[] {size_x - x, 0,          x, y, 50},
                new int[] {0,          size_y - y, x, y, 50},
                new int[] {size_x - x, size_y - y, x, y, 50}
              };
            zoomDataFormat = 1;
            status = "Ok";
        }
    }

    class Sample
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {

               Console.WriteLine("Usage:");
               Console.WriteLine("   example_cs.exe \"Path to KrotW32\" \"zoom_data.json\"");
               Console.WriteLine("Press <ENTER> to terminate the program.");
               Console.ReadLine();

               return;
            }

            string input_file_name = args[1];
            FileStream stream_src = new FileStream(input_file_name, FileMode.Open, FileAccess.Read, FileShare.Read);
            DataContractJsonSerializer deser = new DataContractJsonSerializer(typeof(Answer));
            Answer Answer_src = (Answer)deser.ReadObject(stream_src);
            calc_data return_data = new calc_data();
            return_data.analyze(Answer_src);
            string output_file_name = Path.GetTempFileName() + ".json";
            FileStream stream_dst = new FileStream(output_file_name, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
            //Serialize the Answer object to a stream using DataContractJsonSerializer.
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(calc_data));
            ser.WriteObject(stream_dst, return_data);
            String arguments =" -z" + input_file_name + "!" + output_file_name;
            System.Diagnostics.Process.Start(args[0], arguments);
        }
    }
}
