# -*- coding: utf-8 -*-

"""
Example of python program for KrotW32 zoom data analyzing.
(C) 2016 by Vitaly Bogomolov mail@vitaly-bogomolov.ru
"""
import sys, os, tempfile, json

signature = "zoomDataFormat"
data_section = "dataRow"
min_size = 4 # minimum size (any dimension) of input matrix

class Error(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


def analyze(row_data):
    """
    Analyze passed zoom row data, define 4 objects into source matrix corners
    Size of objects set as 1/4 of dimension of source matrix

    >>> json_valid = '{         \
      "dataType": "MFL",        \
      "dataRow": [              \
        [631, 633, 616, 603],   \
        [631, 632, 615, 603],   \
        [631, 633, 616, 603],   \
        [631, 633, 616, 603]    \
      ],                        \
      "zoomDataFormat": 1       \
    }'
    >>> proc_data(json_valid)
    [[0, 0, 1, 1, 40, 1], [3, 0, 1, 1, 50, 1], [0, 3, 1, 1, 60, 1], [3, 3, 1, 1, 70, 1]]

    >>> json_invalid =          \
      [                         \
        [633, 616, 603],        \
        [632, 615, 603],        \
        [633, 616, 603],        \
      ]
    >>> analyze(json_invalid)
    Traceback (most recent call last):
        ...
    Error: неверный размер исходной матрицы: 3x3 минимальный размер 4
    """
    size_x = len(row_data)
    size_y = len(row_data[0])
    #raw_input("matrix size: %dx%d" % (size_x, size_y))

    # check for minimum matrix size
    if size_y < min_size or size_x < min_size:
        raise Error("неверный размер исходной матрицы: %dx%d минимальный размер %d" % (size_x, size_y, min_size))

    y = size_y / 4
    x = size_x / 4

    return [
        [0,          0,          x, y, 40, 1],
        [size_x - x, 0,          x, y, 50, 1],
        [0,          size_y - y, x, y, 60, 1],
        [size_x - x, size_y - y, x, y, 70, 1],
    ]


def proc_data(json_string):
    """
    Parse passed json string, check for required keys and return results of analyzing for dataRow section

    >>> proc_data('{"broken"')
    Traceback (most recent call last):
    ...
    ValueError: Expecting : delimiter: line 1 column 10 (char 9)

    >>> proc_data('{"dataType": "MFL"}')
    Traceback (most recent call last):
    ...
    Error: signature 'zoomDataFormat' not found in input data

    >>> proc_data('{"dataType": "MFL", "zoomDataFormat": 1}')
    Traceback (most recent call last):
    ...
    Error: section 'dataRow' not found in input data

    """

    data = json.loads(json_string)

    if signature not in data:
        raise Error("signature '%s' not found in input data" % signature)

    if data_section not in data:
        raise Error("section '%s' not found in input data" % data_section)

    return analyze(data[data_section])


def proc_input(input_file_name, krotw32):
    """
    Load input json file, take results of analyzing and save output json file

    >>> proc_input('not_existing_file', 'test.exe')
    [Errno 2] No such file or directory: 'not_existing_file'

    >>> json_invalid = {        \
      "dataType": "MFL",        \
      "dataRow": [              \
        [633, 616, 603],        \
        [632, 615, 603],        \
        [633, 616, 603],        \
      ],                        \
      "zoomDataFormat": 1       \
    }
    >>> input_file = open('input.json', 'w')
    >>> json.dump(json_invalid, input_file, indent=2)
    >>> input_file.close()
    >>> result = proc_input('input.json', '')
    >>> data = json.load(open(result), encoding="utf-8")
    >>> data["status"]
    u'error'
    """
    answer = {signature: 1}

    try:
        data = open(input_file_name, 'r').read()
    except Exception, e:
        print e
        return

    try:
        answer["result"] = proc_data(data)
        answer["status"] = "ok"

    except Error as e:
        answer["status"] = "error"
        answer["error"] = e.message  # .encode("utf-8")

    except Exception as e:
        answer["status"] = "error"
        answer["error"] = str(e)

    output_file = tempfile.NamedTemporaryFile(delete=False)
    output_file_name = output_file.name
    json.dump(answer, output_file, indent=2, ensure_ascii=False, encoding="utf-8")
    output_file.close()

    # call krotw32
    if krotw32:
        os.system("%s -z%s!%s" % (krotw32, input_file_name, output_file_name))

    return output_file_name


if __name__ == "__main__":
    if len(sys.argv) > 2:
        proc_input(sys.argv[1], sys.argv[2])
    else:
        import doctest
        doctest.testmod()
