# Example of python program for KrotW32 zoom data analyzing.
# (C) 2016-2018 by Vitaly Bogomolov mail@vitaly-bogomolov.ru
import os
import tempfile
import json

import mfl

signature = "zoomDataFormat"
data_section = "dataRow"
min_size = 4  # minimum size (any dimension) of input matrix


class Error(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


def proc_data(json_string):
    """
    Parse passed json string, check for required keys and return results of analyzing for dataRow section
    """
    data = json.loads(json_string)

    if signature not in data:
        raise Error("signature '%s' not found in input data" % signature)

    if data_section not in data:
        raise Error("section '%s' not found in input data" % data_section)

    return mfl.analyze(data)


def proc_input(input_file_name, krotw32):
    """
    Load input json file, take results of analyzing and save output json file
    """
    answer = {signature: 1}

    try:
        data = open(input_file_name, 'r').read()
    except Exception, e:
        print e
        return

    try:
        answer["result"] = proc_data(data)
        answer["status"] = "ok"

    except Error as e:
        answer["status"] = "error"
        answer["error"] = e.message  # .encode("utf-8")

    except Exception as e:
        answer["status"] = "error"
        answer["error"] = str(e)

    output_file = tempfile.NamedTemporaryFile(delete=False)
    output_file_name = output_file.name
    json.dump(answer, output_file, indent=2, ensure_ascii=False, encoding="utf-8")
    output_file.close()

    # call krotw32
    if krotw32:
        os.system("%s -z%s!%s" % (krotw32, input_file_name, output_file_name))

    return output_file_name
