import os
import sys

walk_dir = sys.argv[1]

for root, subdirs, files in os.walk(walk_dir):
    for filename in files:
        if filename.endswith(".pyc"):
            file_path = os.path.join(root, filename)
            os.remove(file_path)

has_empty_dirs = True

while has_empty_dirs:
    has_empty_dirs = False
    for root, subdirs, files in os.walk(walk_dir):
        if (not files) and (not subdirs):
            print "remove empty dir:", root
            os.rmdir(root)
            has_empty_dirs = True
            continue
