# python tests.py ../source test.test_depth
from depth import calculate

from . import TestCaseBase


class TestCaseDepth(TestCaseBase):

    def test_calculate(self):
        self.assertEqual(calculate(50, 100, 200, False), 85)
