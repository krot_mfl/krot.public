# python tests.py ../source test.test_mfl
from mfl import analyze, clear_rectangle

from . import TestCaseBase


class TestCaseMFL(TestCaseBase):

    def test_clear_rectangle(self):
        dat = [
          [3, 13, 23, 13],
          [2, 12, 32, 12],
          [3, 23, 23, 23],
          [4, 34, 44, 34],
          [7, 47, 57, 47],
          [3, 33, 43, 33],
          [2, 22, 22, 22],
          [1, 11, 21, 11],
        ]
        clear_rectangle(dat, 2, 1, 3, 2)

        self.assertEqual(dat[0], [3, 13, 23, 13])
        self.assertEqual(dat[1], [2, 12, 32, 12])
        self.assertEqual(dat[2], [3, 13, 23, 23])
        self.assertEqual(dat[3], [4, 13, 23, 34])
        self.assertEqual(dat[4], [7, 13, 23, 47])
        self.assertEqual(dat[5], [3, 33, 43, 33])
        self.assertEqual(dat[6], [2, 22, 22, 22])
        self.assertEqual(dat[7], [1, 11, 21, 11])

    def test_analyze(self):
        dat = {
          "dataType": "MFL",
          "magnetID": "EPRO700",
          "isInside": 0,
          "dataRow": [
            [3, 13, 23, 13],
            [2, 12, 32, 12],
            [3, 23, 23, 23],
            [4, 34, 44, 34],
            [7, 47, 57, 47],
            [3, 33, 43, 33],
            [2, 22, 22, 22],
            [1, 11, 21, 11],
          ],
          "dataFilt": [
            [3, 13, 23, 13],
            [2, 12, 32, 12],
            [3, 23, 23, 23],
            [4, 34, 44, 34],
            [7, 47, 57, 47],
            [3, 33, 43, 33],
            [2, 22, 22, 22],
            [1, 11, 21, 11],
          ],
          "zoomDataFormat": 1,
        }
        self.assertEqual(analyze(dat), [[1, 0, 6, 3, 85, 1]])
