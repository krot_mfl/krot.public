# python tests.py ../source test.test_anomaly
from anomaly import Item

from . import TestCaseBase


class TestCaseAnomaly(TestCaseBase):

    def test_Item(self):
        dat = [
          [3, 13, 23, 13],
          [2, 12, 32, 12],
          [3, 23, 23, 23],
          [4, 34, 44, 34],
          [7, 47, 57, 47],
          [3, 33, 43, 33],
          [2, 22, 22, 22],
          [1, 11, 21, 11],
        ]

        defect = Item(0, dat)

        self.assertEqual(defect.sensor_data, [3, 2, 3, 4, 7, 3, 2, 1])
        self.assertEqual(defect.deltas, [0, -1, 0, 1, 4, 0, -1, -2])
        self.assertEqual(defect.h0, 3)
        self.assertEqual(defect.amplitude, 4)
        self.assertEqual(defect.rectangle(), (2, 0, 3, 1))

        defect = Item(2, dat)

        self.assertEqual(defect.sensor_data, [23, 32, 23, 44, 57, 43, 22, 21])
        self.assertEqual(defect.deltas, [0, 9, 0, 21, 34, 20, -1, -2])
        self.assertEqual(defect.h0, 23)
        self.assertEqual(defect.amplitude, 34)
        self.assertEqual(defect.rectangle(), (2, 0, 4, 3))
