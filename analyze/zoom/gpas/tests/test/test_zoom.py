# python tests.py ../source test.test_zoom
import json

from zoom import Error, proc_data, proc_input

from . import TestCaseBase


class TestCaseZoom(TestCaseBase):

    def test_proc_input(self):
        self.assertEqual(proc_input('not_existing_file', 'test.exe'), None)

        json_invalid = {
          "dataType": "MFL",
          "dataRow": [
            [633, 616, 603],
            [632, 615, 603],
            [633, 616, 603],
          ],
          "zoomDataFormat": 1,
        }
        input_file = open('input.json', 'w')
        json.dump(json_invalid, input_file, indent=2)
        input_file.close()
        result = proc_input('input.json', '')
        data = json.load(open(result), encoding="utf-8")

        self.assertEqual(data["status"], 'error')

    def test_proc_data(self):
        self.assertRaises(ValueError, proc_data, '{"broken"')

        with self.assertRaises(Error) as context:
            proc_data('{"dataType": "MFL"}')
        self.assertEqual("signature 'zoomDataFormat' not found in input data", str(context.exception))

        with self.assertRaises(Error) as context:
            proc_data('{"dataType": "MFL", "zoomDataFormat": 1}')
        self.assertTrue("section 'dataRow' not found in input data" in str(context.exception))
